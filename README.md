- 👋 Hi, I’m Quinn Bellmore
- 👀 I’m interested in Linux
- 🌱 I’m currently learning HTML, C#, C++, Python
- 💞️ I’m looking to collaborate on projects involving linux or customizing window managers 
- 📫 How to reach me: quinn#2978 on Discord

<!---
QuinnB07/QuinnB07 is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
